import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Editor, EditorState, RichUtils, ContentState, convertToRaw, convertFromRaw, CompositeDecorator, getDefaultKeyBinding, KeyBindingUtil } from 'draft-js';
import moment from 'moment';
import IconButton from 'material-ui/IconButton';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import ActionDone from 'material-ui/svg-icons/action/done';
import ActionDelete from 'material-ui/svg-icons/action/delete';
import ActionRestore from 'material-ui/svg-icons/action/restore';
import TextFieldUnderline from 'material-ui/TextField/TextFieldUnderline';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Snackbar from 'material-ui/Snackbar';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import {lightBlue700, redA200} from 'material-ui/styles/colors';

import FormatBold from 'material-ui/svg-icons/editor/format-bold';
import FormatItalic from 'material-ui/svg-icons/editor/format-italic';
import FormatUnderlined from 'material-ui/svg-icons/editor/format-underlined';
import FormatQuote from 'material-ui/svg-icons/editor/format-quote';
import ActionCode from 'material-ui/svg-icons/action/code';
import FormatListBulleted from 'material-ui/svg-icons/editor/format-list-bulleted';
import FormatListNumbered from 'material-ui/svg-icons/editor/format-list-numbered';
import FormatIndentDecrease from 'material-ui/svg-icons/editor/format-indent-decrease';
import FormatIndentIncrease from 'material-ui/svg-icons/editor/format-indent-increase';
import FormatSize from 'material-ui/svg-icons/editor/format-size';
import ContentLink from 'material-ui/svg-icons/content/link';

import { saveNote, deleteNote, selectNote } from '../actions/note_actions';
import Icon from '../components/icon';
import StyleButton from '../components/style_button';
import EditorControl from '../components/editor_control';
import Link from '../components/link';

// Adjust lightBaseTheme for the Editor underline element.
const theme = getMuiTheme(lightBaseTheme);

// Setup function to map command/ctrl+s to save a Note.
const {hasCommandModifier} = KeyBindingUtil;
function saveNoteBindingFn(e: SyntheticKeyboardEvent): string {
  if (e.keyCode === 83 /* `S` key */ && hasCommandModifier(e)) {
    return 'note-save';
  }
  return getDefaultKeyBinding(e);
}


function findLinkEntities(contentBlock, callback, contentState) {
  contentBlock.findEntityRanges(
    (character) => {
      const entityKey = character.getEntity();
      return (
        entityKey !== null &&
        contentState.getEntity(entityKey).getType() === 'LINK'
      );
    },
    callback
  );
}

class Edity extends Component {
  constructor(props) {
    super(props);

    this.decorator = new CompositeDecorator([{strategy: findLinkEntities, component: Link,},]);

    this.state = {
      editorState: EditorState.createEmpty(this.decorator),
      title: '',
      titleError: false,
      isFoucsed: false,
      open: false,
      urlValue: '',
      snack: false,
      snackMessage: '',
      revMenu: false
    }

    this.focus = () => this.refs.editor.focus();

    this.onChange = (editorState) => this.setState({editorState});

    this.handleKeyCommand = (command) => this._handleKeyCommand(command);
    this.onTab = (e) => this._onTab(e);
    this.toggleInlineStyle = (style) => this._toggleInlineStyle(style);
    this.toggleBlockType = (type) => this._toggleBlockType(type);
    this.promptForLink = this._promptForLink.bind(this);
    this.confirmLink = this._confirmLink.bind(this);
    this.onLinkInputKeyDown = this._onLinkInputKeyDown.bind(this);
    this.removeLink = this._removeLink.bind(this);
  }

  _handleKeyCommand(command) {
    const {editorState} = this.state;
    const newState = RichUtils.handleKeyCommand(editorState, command);
    if (command === 'note-save') {
      this.saveEdit();
      return 'handled';
    }
    if (newState) {
      this.onChange(newState);
      return true;
    }
    return false;
  }

  _onTab(e) {
    const maxDepth = 4;
    this.onChange(RichUtils.onTab(e, this.state.editorState, maxDepth));
  }

  _toggleInlineStyle(inlineStyle) {
    this.onChange(RichUtils.toggleInlineStyle(this.state.editorState, inlineStyle));
  }

  _toggleBlockType(blockType) {
    this.onChange(RichUtils.toggleBlockType(this.state.editorState, blockType));
  }

  _promptForLink(e) {
    e.preventDefault();
    const {editorState} = this.state;
    const selection = editorState.getSelection();
    if (!selection.isCollapsed()) {
      const contentState = editorState.getCurrentContent();
      const startKey = editorState.getSelection().getStartKey();
      const startOffset = editorState.getSelection().getStartOffset();
      const blockWithLinkAtBeginning = contentState.getBlockForKey(startKey);
      const linkKey = blockWithLinkAtBeginning.getEntityAt(startOffset);

      let url = '';
      if (linkKey) {
        const linkInstance = contentState.getEntity(linkKey);
        url = linkInstance.getData().url;
      }

      this.setState({
        open: true,
        urlValue: url,
      }, () => {
        setTimeout(() => this.refs.url.focus(), 0);
      });
    }
  }

  _confirmLink(e) {
    e.preventDefault();
    const {editorState, urlValue} = this.state;
    const contentState = editorState.getCurrentContent();
    const contentStateWithEntity = contentState.createEntity(
      'LINK',
      'MUTABLE',
      {url: urlValue}
    );
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const newEditorState = EditorState.set(editorState, { currentContent: contentStateWithEntity });
    this.setState({
      editorState: RichUtils.toggleLink(
        newEditorState,
        newEditorState.getSelection(),
        entityKey
      ),
      open: false,
      urlValue: '',
    }, () => {
      setTimeout(() => this.refs.editor.focus(), 0);
    });
  }

  _removeLink(e) {
    e.preventDefault();
    const {editorState} = this.state;
    const selection = editorState.getSelection();
    if (!selection.isCollapsed()) {
      this.setState({editorState: RichUtils.toggleLink(editorState, selection, null), open: false, urlValue: ''}, () => {
        setTimeout(() => this.refs.editor.focus(), 0);
      });
    }
  }

  _onLinkInputKeyDown(e) {
    if (e.which === 13) {
      this._confirmLink(e);
    }
  }

  componentWillMount() {
    if (this.props.note) {
      const content = convertFromRaw(this.props.note.doc.body);
      this.setState({editorState: EditorState.createWithContent(content, this.decorator)});
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.note.doc.revNotFound !== undefined) {
      this.setState({snack: true, snackMessage: 'Revision: ' + nextProps.note.doc.revNumber + ' could not be found.'}, () => {});
      nextProps.selectNote({doc: {_id: nextProps.note.doc._id}})
    } else {
      if (this.props.note) {
        const decorator = new CompositeDecorator([{strategy: findLinkEntities, component: Link,},]);
        if (nextProps.addNote) {
          this.setState({editorState: EditorState.createEmpty(decorator)});
        } else {
          const content = convertFromRaw(nextProps.note.doc.body);
          this.setState({editorState: EditorState.createWithContent(content, decorator)});
        }
      }
    }
  }

  handleTitleChange(e) {
    e.preventDefault();
    this.setState({title: e.target.value, titleError: false});
  }

  saveEdit(e) {
    if (e) {
      e.preventDefault();
    }

    let note;
    if (this.props.note && !this.props.addNote) {
      note = this.props.note.doc;
    } else {
      note = {};
    }

    if (this.state.title !== '') {
      note.title = this.state.title;
    }

    // Don't save a note without a title.
    let titleError;
    if (note.title == '' || note.title == undefined) {
      this.setState({titleError: !this.state.titleError});
    } else {
      titleError = undefined;
      note.folder = this.props.folder ? this.props.folder._id : 'main';
      note.body = convertToRaw(this.state.editorState.getCurrentContent());
      this.props.saveNote(note);
      this.props.addedNote();
      this.setState({snack: true, snackMessage: "Note saved..."});
    }
  }

  handleLink() {
    const contentState = this.state.editorState.getCurrentContent();
    const startKey = this.state.editorState.getSelection().getStartKey();
    const startOffset = this.state.editorState.getSelection().getStartOffset();
    const blockWithLinkAtBeginning = contentState.getBlockForKey(startKey);
    const linkKey = blockWithLinkAtBeginning.getEntityAt(startOffset);

    let url = '';
    if (linkKey) {
      const linkInstance = contentState.getEntity(linkKey);
      url = linkInstance.getData().url;
    }

    this.setState({open: true, urlValue: url});
  }

  changeRev(idx, el, rev) {
    this.setState({revMenu: false});
    this.props.selectNote({doc: {_id: this.props.note.doc._id, getRev: idx + '-' + rev}});
  }

  render() {
    let addNote;
    if (this.props.addNote) {
      addNote = (
        <div>

          {this.state.titleError ?
            <TextField
              hintText=""
              floatingLabelText="Title"
              name="title"
              value={this.state.title}
              fullWidth={true}
              ref='title'
              errorText='Please add a title.'
              onChange={this.handleTitleChange.bind(this)}
              underlineFocusStyle={{borderColor: lightBlue700}}
            />
           :
            <TextField
              hintText=""
              floatingLabelText="Title"
              name="title"
              value={this.state.title}
              fullWidth={true}
              ref='title'
              onChange={this.handleTitleChange.bind(this)}
              underlineFocusStyle={{borderColor: lightBlue700}}
            />
          }
          <br/><br/>
        </div>
      );
    }

    const HEADERS = [
      {label: 'H1', style: 'header-one'},
      {label: 'H2', style: 'header-two'},
      {label: 'H3', style: 'header-three'},
      {label: 'H4', style: 'header-four'},
      {label: 'H5', style: 'header-five'},
      {label: 'H6', style: 'header-six'},
    ]

    const BLOCK_TYPES = [
      {label: 'quote', style: 'blockquote', button: <FormatQuote />},
      {label: 'code', style: 'code-block', button: <ActionCode />},
    ];

    const DENTS = [
      {label: 'indent', style: 'indent', button: <FormatIndentIncrease />},
      {label: 'outdent', style: 'outdent', button: <FormatIndentDecrease />},
    ]

    const LIST_TYPES = [
      {label: 'ordered-list', style: 'ordered-list-item', button: <FormatListNumbered />},
      {label: 'unordered-list', style: 'unordered-list-item', button: <FormatListBulleted />},
    ]

    var INLINE_STYLES = [
      {label: 'bold', style: 'BOLD', button: <FormatBold />},
      {label: 'italic', style: 'ITALIC', button: <FormatItalic />},
      {label: 'underline', style: 'UNDERLINE', button: <FormatUnderlined />},
    ];

    let lastSaved;
    if (this.props.note) {
      lastSaved = (
        <div className="sync-date">
          Last saved: <strong>{moment.unix(this.props.note.doc.updatedAt).fromNow()}</strong>
          &nbsp;&nbsp;
          Current Revision: <strong>{this.props.note.doc._rev}</strong>
        </div>
      );
    } else {
      lastSaved = '';
    }

    const actions = [
      <FlatButton
        label="Remove Link"
        primary={true}
        onTouchTap={this.removeLink}
      />,
      <FlatButton
        label="Save Link"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.confirmLink}
      />,
    ];

    let revisions;
    if (this.props.note) {
      if (this.props.note.doc._revisions) {
        const revisionMenuItems = this.props.note.doc._revisions.ids.map((rev, idx) => {
          return (
            <MenuItem key={rev} primaryText={rev} onClick={() => this.changeRev(idx, this, rev)} />
          );
        });
        revisions = (
          <Popover
            open={this.state.revMenu}
            anchorEl={this.state.anchorEl}
            anchorOrigin={{horizontal: 'left', vertical: 'top'}}
            targetOrigin={{horizontal: 'right', vertical: 'top'}}
            onRequestClose={() => this.setState({revMenu: false})}
          >
            <Menu>
              {revisionMenuItems}
            </Menu>
          </Popover>
        );
      }
    }

    return (
      <div className="editor">
        {addNote}

        <Divider />
          <EditorControl type={'inline'} editorState={this.state.editorState} onToggle={this.toggleInlineStyle} actions={INLINE_STYLES} />
          <EditorControl type={'block'} editorState={this.state.editorState} onToggle={this.toggleBlockType} actions={LIST_TYPES} />
          <EditorControl type={'block'} editorState={this.state.editorState} onToggle={this.toggleBlockType} actions={BLOCK_TYPES} />
          <EditorControl type={'header'} editorState={this.state.editorState} onToggle={this.toggleBlockType} actions={HEADERS} />

          <div className="RichEditor-controls">
            <IconButton onTouchTap={this.handleLink.bind(this)}><ContentLink /></IconButton>
          </div>

          <Dialog
            title="Add Link"
            actions={actions}
            modal={false}
            open={this.state.open}
            onRequestClose={() => this.setState({open: false})}
          >
          <TextField
            hintText=""
            floatingLabelText="URL"
            name="url"
            value={this.state.urlValue}
            onChange={(e) => {this.setState({urlValue: e.target.value})}}
            onKeyDown={this.onLinkInputKeyDown}
            underlineFocusStyle={{borderColor: lightBlue700}}
          />
          </Dialog>
        <Divider />

        <br/><br/>

        <div onClick={this.focus} onFocus={() => this.setState({isFocused: true})} onBlur={() => this.setState({isFocused: false})}>

          <Editor
            editorState={this.state.editorState}
            handleKeyCommand={this.handleKeyCommand}
            onChange={this.onChange}
            onTab={this.onTab.bind(this)}
            spellCheck={true}
            ref="editor"
            keyBindingFn={saveNoteBindingFn}
          />

          <TextFieldUnderline
            disabled={false}
            focus={this.state.isFocused}
            focusStyle={{position: 'static', bottom: 'inherit'}}
            style={{position: 'static', bottom: 'inherit'}}
            muiTheme={getMuiTheme(lightBaseTheme)}
            focusStyle={{borderColor: lightBlue700}}
          />
        </div>
        <br/>

        <IconButton onClick={this.saveEdit.bind(this)}>
          <ActionDone />
        </IconButton>

        &nbsp;&nbsp;&nbsp;&nbsp;


        {this.props.note && !this.props.addNote
          ? (<span>
              <IconButton onClick={(event) => this.setState({revMenu: !this.state.revMenu, anchorEl: event.currentTarget})}>
                {revisions ? revisions : ''}
                <ActionRestore />
              </IconButton>

              &nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;

              <IconButton onClick={() => this.props.deleteNote(this.props.note.doc)}>
                <ActionDelete color={redA200} />
              </IconButton>
            </span>)
          : ''}

          &nbsp;&nbsp;&nbsp;&nbsp;
          {lastSaved}

          <Snackbar
            open={this.state.snack}
            message={this.state.snackMessage}
            autoHideDuration={2000}
            onRequestClose={() => this.setState({snack: false, snackMessage: ''})}
          />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return { note: state.app.activeNote, folder: state.app.activeFolder };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    saveNote: saveNote,
    deleteNote: deleteNote,
    selectNote: selectNote
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Edity);
