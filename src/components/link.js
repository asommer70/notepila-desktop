import React from 'react';
import electron from 'electron';

export default (props) => {
  const {url} = props.contentState.getEntity(props.entityKey).getData();
  return (
    <a href={url} className='editor-link' onClick={() => electron.shell.openExternal(url)}>
      {props.children}
    </a>
  );
}
