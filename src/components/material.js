import React from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';

import App from '../containers/app';

import {
  grey100, grey500, grey400, grey900, grey600,
  blueGrey700, blueGrey300, blueGrey500, blueGrey900,
  redA200,
  lightBlue500, indigo400,
  fullBlack, darkBlack, white
} from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {fade} from 'material-ui/utils/colorManipulator';

const theme = getMuiTheme({
  palette: {
    primary1Color: blueGrey700,
    primary2Color: grey500,
    primary3Color: indigo400,
    accent1Color: redA200,
    accent2Color: indigo400,
    accent3Color: lightBlue500,
    textColor: blueGrey900,
    alternateTextColor: grey600,
    alternateTextColor: white,
    canvasColor: white,
    borderColor: grey400,
    disabledColor: fade(darkBlack, 0.3),
    pickerHeaderColor: blueGrey500,
    clockCircleColor: fade(darkBlack, 0.07),
    shadowColor: fullBlack,
  }
});

injectTapEventPlugin();

export default () => (
  <MuiThemeProvider muiTheme={theme}>
    <App />
  </MuiThemeProvider>
);
