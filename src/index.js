import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import PouchDB from 'pouchdb';
import thunk from 'redux-thunk';
import electron from 'electron';
import SpellCheckProvider from 'electron-spell-check-provider';

import './css/simple-grid.css';
import './css/main.css';
import './css/fonts.css';
import './css/draft-default.css';
import './css/rich-editor.css';
import './css/media-queries.css';

import reducers from './reducers';
import Folders from './containers/folders';
import Notes from './containers/notes';
import App from './containers/app';
import Material from './components/material';

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);

// Spell Checking.
const remote = electron.remote;
const webFrame = electron.webFrame;
webFrame.setSpellCheckProvider('en-US', true, new SpellCheckProvider('en-US'));
var buildEditorContextMenu = remote.require('electron-editor-context-menu');

var selection;
function resetSelection() {
  selection = {
    isMisspelled: false,
    spellingSuggestions: []
  };
}
resetSelection();

// Reset the selection when clicking around, before the spell-checker runs and the context menu shows.
// window.addEventListener('mousedown', resetSelection);

webFrame.setSpellCheckProvider(
  'en-US',
  true,
  new SpellCheckProvider('en-US').on('misspelling', function(suggestions) {
    if (window.getSelection().toString()) {
      selection.isMisspelled = true;
      selection.spellingSuggestions = suggestions.slice(0, 3);
    }
  })
);

window.addEventListener('contextmenu', function(e) {
  if (!e.target.closest('textarea, input, [contenteditable="true"]')) return;

  var menu = buildEditorContextMenu(selection);

  setTimeout(function() {
    menu.popup(remote.getCurrentWindow());
  }, 30);
});

ReactDOM.render(<Provider store={createStoreWithMiddleware(reducers)}>
                  <Material />
                </Provider>, document.getElementById('app'));
