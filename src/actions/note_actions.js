import slugify from 'slugify';
import { db } from './index';
import moment from 'moment';

export const LIST_NOTES = 'list_notes';
export const SELECT_NOTE = 'select_note';
export const SELECT_NOTE_NOTFOUND = 'select_note_notfound';
export const ADD_NOTE = 'add_note';
export const DELETE_NOTE = 'delete_note';
export const UPDATE_NOTE = 'update_note';

export function listNotes(folder) {
  const query = db.query('notes/index', {
    key: folder,
    include_docs: true,
    descending: true
  })

  return (dispatch) => {
    query.then((data) => {
      dispatch({
        type: LIST_NOTES,
        payload: data.rows
      })
    })
  }
}

export function selectNote(note) {
  let query;
  if (note.doc.getRev) {
    query = db.get(note.doc._id, {revs: true, rev: note.doc.getRev});
  } else {
    query = db.get(note.doc._id, {revs: true});
  }
  return (dispatch) => {
    query.then((data) => {
      dispatch({
        type: SELECT_NOTE,
        payload: {doc: data}
      });
    }).catch((err) => {
      dispatch({
        type: SELECT_NOTE_NOTFOUND,
        payload: {doc: {_id: note.doc._id, revNotFound: note.doc.getRev, revNumber: note.doc.getRev.split('-')[0]}}
      });
    })
  }
}

export function saveNote(note) {
  let type, newNote;
  let bodyRaw = '';

  note.body.blocks.map((block) => {
    bodyRaw += block.text + "\n";
  });

  if (!note._rev) {
    newNote = {
      _id: slugify(note.title, '_').toLowerCase(),
      title: note.title,
      body: note.body,
      bodyRaw: bodyRaw,
      folder: note.folder,
      type: 'note',
      createdAt: moment().unix(),
      updatedAt: moment().unix()
    }
    type = ADD_NOTE;
  } else {
    newNote = note;
    newNote.bodyRaw = bodyRaw;
    newNote.updatedAt = moment().unix();
    type = UPDATE_NOTE;
  }

  let query;
  if (type == ADD_NOTE) {
    query = db.putIfNotExists(newNote);
  } else {
    query = db.upsert(newNote._id, (doc) => {
      return newNote;
    });
  }

  return (dispatch) => {
    query.then((data) => {
      return db.get(newNote._id, {revs: true}).then((doc) => {
        dispatch({
          type: type,
          payload: doc
        });
        dispatch({
          type: SELECT_NOTE,
          payload: {doc: doc}
        });
      });
    }).catch((err) => {
      console.log('saveNote err:', err);
    });
  }
}

export function deleteNote(note) {
  const query = db.remove(note);

  return (dispatch) => {
    query.then((data) => {
      dispatch({
        type: DELETE_NOTE,
        payload: {data: data, deleted: data.id}
      });

      dispatch({
        type: SELECT_NOTE,
        payload: undefined
      });
    })
  }
}

export function search(term) {
  const query = db.search({
    query: term,
    fields: ['title', 'bodyRaw'],
    include_docs: true,
    filter: (doc) => {
      return doc.type === 'note';
    },
    mm: '33%'
  });

  return (dispatch) => {
    query.then((data) => {
      dispatch({
        type: LIST_NOTES,
        payload: data.rows
      });
    }).catch((err) => {
      console.log('search err:', err);
    });
  }
}
