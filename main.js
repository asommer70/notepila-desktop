const electron = require('electron');
const Menu = electron.Menu;
const ipcMain = electron.ipcMain

const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
let mainWindow;

function createWindow (width, height) {
  mainWindow = new BrowserWindow({
    width: width,
    height: height,
    minWidth: 640,
    minHeight: 640,
    frame: false,
    title: 'Note Pila!',
  })

  mainWindow.loadURL(`file://${__dirname}/dist/index.html`)

  // mainWindow.webContents.openDevTools()
  mainWindow.on('closed', function () {
    mainWindow = null
  })
}

app.on('ready', () => {
  // const sizes = electron.screen.getPrimaryDisplay().workAreaSize;

  createWindow(640, 640);

  // Create the Application's main menu
  var template = [{
    label: "Note Pila!",
    submenu: [
      { label: "About Application", selector: "orderFrontStandardAboutPanel:" },
      { type: "separator" },
      { label: "Quit", accelerator: "Command+Q", click: function() { app.quit(); }}
    ]}, {
    label: "Edit",
    submenu: [
        { label: "Undo", accelerator: "CmdOrCtrl+Z", selector: "undo:" },
        { label: "Redo", accelerator: "Shift+CmdOrCtrl+Z", selector: "redo:" },
        { type: "separator" },
        { label: "Cut", accelerator: "CmdOrCtrl+X", selector: "cut:" },
        { label: "Copy", accelerator: "CmdOrCtrl+C", selector: "copy:" },
        { label: "Paste", accelerator: "CmdOrCtrl+V", selector: "paste:" },
        { label: "Select All", accelerator: "CmdOrCtrl+A", selector: "selectAll:" }
    ]},  {
    label: 'View',
    submenu: [
      {
        label: 'Reload',
        accelerator: 'CmdOrCtrl+R',
        click (item, focusedWindow) {
          if (focusedWindow) focusedWindow.reload()
        }
      },
      {
        label: 'Toggle Developer Tools',
        accelerator: process.platform === 'darwin' ? 'Alt+Command+I' : 'Ctrl+Shift+I',
        click (item, focusedWindow) {
          if (focusedWindow) focusedWindow.webContents.toggleDevTools()
        }
      },
    ]}
  ];
  Menu.setApplicationMenu(Menu.buildFromTemplate(template));
})

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow()
  }
})

ipcMain.on('big-screen', (event, arg) => {
  const sizes = electron.screen.getPrimaryDisplay().workAreaSize;
  const currentSizes = mainWindow.getContentSize();

  let width, height, x;
  if (currentSizes[0] === sizes.width && sizes.height === currentSizes[1]) {
    width = 640;
    height = 640;
    x = 500
  } else {
    width = sizes.width;
    height = sizes.height;
    x = 0;
  }

  mainWindow.setBounds({
      width: width,
      height: height,
      x: x,
      y: 0
  });
})
