# Note Pila! Desktop

Desktop version of [Note Pila!](https://github.com/asommer70/notepila) developed using [Electron](http://electron.atom.io/).

Also, uses [React](https://facebook.github.io/react/) and [PouchDB](https://pouchdb.com/) as the backend.

## Purpose

Be a simple note taking app that can sync to a PouchDB server and run as a stand alone desktop app.

## Installation

Clone the source:

```
git clone https://gitlab.com/asommer70/notepila-desktop.git
```

Install the dependencies:

```
cd notepila-desktop
npm install
```

Build the app (on Mac):

```
npm run build:pro:mac
```

Double click on the **NotePila-darwin-x64/NotePila.app** and the app should fire up and you can take some wonderful notes.  Woo!
